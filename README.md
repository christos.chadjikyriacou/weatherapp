# Readme

# Design

For design I tried to follow Apple guidelines and components a much a possible. You can see this from first page where I use Large Navigation bar title like Apple apps. The app support iPhone and iPad platforms, Light and Dark appearance and different orientations.

# Implementation


**UI Implementation**

UI was built using auto layout and apples Interface Builder. I didn't use storyboard because they are vert slow to load and makes it hard to find the design for a specific view controller. Also it creates problems when merging two different branches.

For tableView and collections view I used the new diffable datasources that apple presented last year.

**Architecture**

The app was build using the MVVM architecture using Apple's combine. Repositories are responsible for providing data and view models are where the business logic goes. A view model can use many repositories but each view controller needs to have its own view model. This creates a better separation of concerns

 I created a protocol based Dependency Injection library that injects the repositories and viewModels where are needed. This is helpful for testing purposes because the library is responsible for the initialisation of some components that view controllers are using. Also this makes it easier to work with mock data and create mock based test cases.

**Third Party Libraries**

TrustKit is an open source framework that makes it easy to deploy SSL public key pinning and reporting so the user can be safe from man-in-the-middle attacks.

SwiftGen is a tool to automatically generate Swift code for resources of your projects (like images, localised strings, etc), to make them type-safe to use.


# Testing

I created some unit tests too but for this to be a finished implementation there is a need for a lot more test cases
