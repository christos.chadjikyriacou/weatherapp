//
//  CityTableViewCell.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    private var city:CityModel! {
        didSet {
            cityNameLabel.text = city.fullName
        }
    }
    
    @IBOutlet weak var cityNameLabel: UILabel!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cityNameLabel.text = nil
    }
    
    static func create(tableView:UITableView,city:CityModel) -> CityTableViewCell{
        return CityTableViewCell
            .create(tableView: tableView)
            .apply(closure: {$0.city = city})
    }
    
}
