//
//  Endpoints.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


class Endpoints {
    
    static let host = "https://api.worldweatheronline.com"
    
    static func weather(location:CityModel,numOfDays:Int = 5) -> String {
        
        host + "/premium/v1/weather.ashx?" +
            "key=\(Keys.apiKey)&" +
            "q=\(location.latitude),\(location.longitude)&" +
            "num_of_days=\(numOfDays)&" +
            "format=json&extra=isDayTime&" +
            "fx=yes&" +
        "show_comments=no"
        
    }
    
    static func search(query:String) -> String {
        host + "/premium/v1/search.ashx?" +
            "key=\(Keys.apiKey)&" +
            "q=\(query)&" +
        "format=json"
        
    }
    
    
    
    
}
