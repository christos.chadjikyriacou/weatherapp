//
//  AddCitiesViewModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import Combine

protocol AddCitiesViewModel:RequestViewModel {
    func search(query:String)
    func select(city:CityModel)
    
    var searchResultSubject:PassthroughSubject<[CityModel],Never> {get}
}

class AddCitiesViewModelImpl:RequestViewModelImpl,AddCitiesViewModel {
    @Inject
    private var citiesRepository:CitiesRepository
    
    func select(city:CityModel) {
        citiesRepository.select(city: city)
    }
    
    var searchResultSubject = PassthroughSubject<[CityModel],Never>()
    
    
    
    func search(query:String)  {
        isLoadingSubject.send(true)
        citiesRepository.search(query: query) { (result) in
            self.isLoadingSubject.send(false)
            switch result {
            case .failure(let error):
                self.errorSubject.send(error)
            case .success(let response):
                self.searchResultSubject.send(response.data.cities)
            }
            
        }
    }
    
    
    
}
