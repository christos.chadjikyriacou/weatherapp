//
//  AddCityViewController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 07/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class AddCityViewController: ModalBaseViewController {
    
  
    
    
    
    private enum Section {
        case main
    }
    
    @Inject
    private var viewModel:AddCitiesViewModel
    
    @IBOutlet weak private var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var tableView: UITableView! {
        willSet {
            newValue.separatorStyle = .none
            newValue.delegate = self
            
        }
        
        didSet {
            tableView.dataSource = datasource
        }
    }
    
    private lazy var activityIndicator = UIActivityIndicatorView(style: .medium).apply(closure: {$0.hidesWhenStopped = true})
    private lazy var activityItem = UIBarButtonItem(customView: activityIndicator)
    
    override func done() {
        UIView.performWithoutAnimation {
            searchController.isActive = false
        }
        super.done()
    }
    
    
    private lazy var searchController = UISearchController(searchResultsController:  nil).apply(closure: {
        $0.title = "Test"
        $0.searchResultsUpdater = self
        $0.delegate = self
        $0.searchBar.delegate = self
        $0.hidesNavigationBarDuringPresentation = false
        $0.obscuresBackgroundDuringPresentation = false
        $0.automaticallyShowsCancelButton = false
    })
    
    
    private var cities = [CityModel]() {
        willSet {
            apply(cities: newValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showDoneItem = true
        self.navigationItem.title = L10n.addCity
        self.navigationItem.setRightBarButton(activityItem, animated: true)
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true
    }
    
    private lazy var datasource = UITableViewDiffableDataSource<Section,CityModel>(tableView: tableView) { (tableView,_,city) -> UITableViewCell? in
        CityTableViewCell.create(tableView: tableView, city: city)
    }
    
    private lazy var snapshot = NSDiffableDataSourceSnapshot<Section,CityModel>()
    

    private func apply(cities:[CityModel]) {
        snapshot.deleteAllItems()
        snapshot.appendSections([.main])
        snapshot.appendItems(cities, toSection: .main)
        datasource.apply(snapshot)
    }
    
    
    override func configureObservers() {
        super.configureObservers()
        
        viewModel.searchResultSubject
            .assign(to: \.cities, on: self)
            .store(in: &cancellables)
        
        viewModel.isLoadingSubject
            .assign(to: \.isLoading, on: activityIndicator)
            .store(in: &cancellables)
    }
    
    override func onKeyboardStateChange(willOpen: Bool, height: CGFloat, duration: TimeInterval) {
        tableViewBottomConstraint.constant = willOpen ? height : 0
        UIView.animate(withDuration: duration) {
            self.view.layoutIfNeeded()
        }
    }

}


extension AddCityViewController:UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate  {
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.search(query: searchController.searchBar.text ?? "")
    }
    
    

}


extension AddCityViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let city = datasource.itemIdentifier(for: indexPath) else { return }
        viewModel.select(city: city)
        done()
    }
}
