//
//  BaseNavigationController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 05/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class BaseNavigationController:UINavigationController {
    
    func addCity(onDismiss:@escaping ()->Void) {
        let controller = AddCityNavigationController(onDismiss:onDismiss)
          present(controller, animated: true, completion: nil)
      }
      
      func editCities(onDismiss:@escaping ()->Void) {
        let controller = EditCitiesNavigationController(onDismiss:onDismiss)
          present(controller, animated: true, completion: nil)
      }
}
