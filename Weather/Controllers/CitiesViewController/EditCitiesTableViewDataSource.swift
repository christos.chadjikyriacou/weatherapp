//
//  EditCitiesTableViewDataSource.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 11/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit


class EditCitiesTableViewDataSource:UITableViewDiffableDataSource<EditCitiesTableViewDataSource.Section,CityModel> {
    enum Section {
        case main
    }
    
    private let onDeleteCommit:(CityModel) -> Void
    
    init(tableView: UITableView,onDeleteCommit:@escaping (CityModel) -> Void) {
        self.onDeleteCommit = onDeleteCommit
        super.init(tableView: tableView) { (tableView,_, city) -> UITableViewCell? in
            CityTableViewCell.create(tableView: tableView, city: city)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard let city = itemIdentifier(for: indexPath) else { return }
        onDeleteCommit(city)
    }
}
