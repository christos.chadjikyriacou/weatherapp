//
//  CitiesViewController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 05/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class EditCitiesViewController: ModalBaseViewController {
    
    
    
    @Inject
    private var viewModel:EditCitiesViewModel
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = datasource
        }
        
        willSet {
            newValue.separatorStyle = .none
            newValue.isEditing = true
            newValue.delegate = self
        }
    }
    
    override var navigationController: EditCitiesNavigationController? {
        return super.navigationController as? EditCitiesNavigationController
    }
    
    
    private lazy var addCityItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showAddCity))
    
    
    @objc private func showAddCity() {
        navigationController?.addCity(onDismiss: viewModel.getSelectedCities)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showDoneItem = true
        navigationItem.title = L10n.editCities
        navigationItem.setRightBarButton(addCityItem, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.getSelectedCities()
    }
    override func configureObservers() {
        super.configureObservers()
        
        viewModel.selectedCitiesSubject
            .assign(to: \.cities, on: self)
            .store(in: &cancellables)
    }
    
    
    private var cities = [CityModel]() {
        willSet {
            reloadSnapshot(cities: newValue)
        }
    }
    
    
    private lazy var datasource = EditCitiesTableViewDataSource(tableView: tableView) { (city) in
        do {
            try self.viewModel.unselectCity(city: city)
            self.deleteCityInSnapshot(city: city)
        }
        catch {
            self.showError(error: error)
        }
        
    }
    
    
    
    private func deleteCityInSnapshot(city:CityModel) {
        var snapshot = datasource.snapshot()
        snapshot.deleteItems([city])
        datasource.apply(snapshot,animatingDifferences: true)
    }
    
    private func reloadSnapshot(cities:[CityModel]) {
        var snapshot = NSDiffableDataSourceSnapshot<EditCitiesTableViewDataSource.Section,CityModel>()
        snapshot.appendSections([.main])
        snapshot.appendItems(cities, toSection: .main)
        datasource.apply(snapshot,animatingDifferences: false)
    }
    
    
}




extension EditCitiesViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    
    
}
