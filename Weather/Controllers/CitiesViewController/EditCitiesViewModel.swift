//
//  EditCitiesViewModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 09/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import Combine

protocol EditCitiesViewModel:RequestViewModel {
    func unselectCity(city:CityModel) throws
    func getSelectedCities()
    
    var selectedCitiesSubject:PassthroughSubject<[CityModel],Never>{get}
}

class EditCitiesViewModelImpl:RequestViewModelImpl,EditCitiesViewModel {
    
       @Inject
       var citiesRepository:CitiesRepository
       
       var selectedCitiesSubject = PassthroughSubject<[CityModel],Never>()
           
       
       func getSelectedCities() {
           selectedCitiesSubject.send(citiesRepository.allSelectedCities)
           
       }
    
    
    func unselectCity(city:CityModel) throws {
        try citiesRepository.unselect(city: city)
    }
    
}
