//
//  DayWeatherCollectionReusableView.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 11/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class DayWeatherCollectionReusableView: UICollectionReusableView {

    static let kind = "day-section-header-element-kind"
    private var data:DayWeatherModel! {
        willSet {
            if newValue.date.isToday {
                dayLabel.text = L10n.today
            }
            else if newValue.date.isTomorrow {
                dayLabel.text = L10n.tomorrow
            }
            else {
                dayLabel.text = newValue.date.dayOfWeek
            }
       
            tempLabel.text = newValue.avgTemp.toCelciusString
        }
    }
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    static func create(collectionView:UICollectionView,indexPath:IndexPath,data:DayWeatherModel) -> DayWeatherCollectionReusableView {
        return DayWeatherCollectionReusableView
            .create(collectionView: collectionView,kind: kind, indexPath: indexPath)
            .apply(closure: {$0.data = data})
    }
}
