//
//  HourWeatherCollectionViewCell.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 11/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class HourWeatherCollectionViewCell: UICollectionViewCell {

    
    private var data:HourWeatherModel! {
        willSet {
            hourLabel.text = newValue.time
            weatherIconImageView.image = newValue.weatherType?.image(isDayTime: newValue.isDayTime)
            tempLabel.text = newValue.temp.toCelciusString
        }
    }
    
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    
    static func create(collectionView:UICollectionView,indexPath:IndexPath,data:HourWeatherModel) -> HourWeatherCollectionViewCell {
        return HourWeatherCollectionViewCell
            .create(collectionView: collectionView, indexPath: indexPath)
            .apply(closure: {$0.data = data})
    }

}
