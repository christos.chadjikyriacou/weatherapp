//
//  HourWeatherMainCollectionViewCell.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 11/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class HourWeatherMainCollectionViewCell: UICollectionViewCell {


    private var data:HourWeatherModel! {
        willSet {
            weatherIconImageView.image = newValue.weatherType?.image(isDayTime: newValue.isDayTime)
            weatherDescLabel.text = newValue.desc
            tempLabel.text = newValue.temp.toCelciusString
        }
    }
    
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var weatherDescLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        weatherIconImageView.image = nil
        weatherDescLabel.text = nil
        tempLabel.text = nil
    }
    
    static func create(collectionView:UICollectionView,indexPath:IndexPath,data:HourWeatherModel) -> HourWeatherMainCollectionViewCell {
        return HourWeatherMainCollectionViewCell
            .create(collectionView: collectionView, indexPath: indexPath)
            .apply(closure: {$0.data = data})
    }

}
