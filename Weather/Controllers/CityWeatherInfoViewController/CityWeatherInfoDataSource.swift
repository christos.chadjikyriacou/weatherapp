//
//  CityWeatherInfoDataSource.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 11/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit


class CityWeatherInfoDataSource:UICollectionViewDiffableDataSource<CityWeatherInfoDataSource.Section,HourWeatherModel>   {
    
    
    enum Section:Hashable {
        case main
        case day(data:DayWeatherModel)
    }
    
    
 
    init(collectionView:UICollectionView) {
        super.init(collectionView: collectionView) { (collectionView, indexPath, data) -> UICollectionViewCell? in
            if indexPath.section == 0 {
                return HourWeatherMainCollectionViewCell.create(collectionView: collectionView, indexPath: indexPath, data: data)
            }
            return HourWeatherCollectionViewCell.create(collectionView: collectionView, indexPath: indexPath, data: data)
            
        }
        
        supplementaryViewProvider = { collectionView, kind, indexPath in
         
            guard let data = self.snapshot().sectionIdentifiers[safe:indexPath.section] else {
                    return nil
            }
            switch  data  {
            case .day(let dayData):
                return DayWeatherCollectionReusableView.create(collectionView: collectionView, indexPath: indexPath, data: dayData)
            case .main:
                return nil
            }

        
        }
        
    }
    
   
    
    
    
    
    
    
}
