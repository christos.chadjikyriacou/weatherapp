//
//  CityWeatherInfoViewController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 05/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class CityWeatherInfoViewController: BaseViewController {
    
    
    
    @Inject
    private var viewModel:CityWeatherInfoViewModel
    
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = dataSource
        }
        
        willSet {
            newValue.collectionViewLayout = layout
            newValue.refreshControl = refershControl
        }
    }
    
    private lazy var refershControl:UIRefreshControl = {
        let refershControl = UIRefreshControl(frame: .zero)
        refershControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refershControl
    }()
    
    
    private var weather:CityWeatherModel? {
        willSet {
            guard let weather = newValue else { return }
            applySnapshot(weather: weather)
        }
    }
    let city:CityModel
    
    init(city:CityModel) {
        self.city = city
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refresh()
    }
    
    @objc private func refresh() {
        viewModel.getWeather(city: city)
    }
    
    override func configureObservers() {
        super.configureObservers()
        viewModel.isLoadingSubject.sink { (isLoading) in
            isLoading ? self.refershControl.beginRefreshing() : self.refershControl.endRefreshing()
        }.store(in: &cancellables)
        
        viewModel.weatherSubject.sink { (weather) in
            self.weather = weather
        }.store(in: &cancellables)
    }
    
    private lazy var dataSource = CityWeatherInfoDataSource(collectionView: collectionView)
    
    
    private lazy var layout:UICollectionViewCompositionalLayout = {
        UICollectionViewCompositionalLayout { (sectionIndex, layoutEnviroment) -> NSCollectionLayoutSection? in
            guard sectionIndex != 0 else {
                return self.mainSectionLayout()
            }
            
            return self.daysSectionLayout()
        }
        
    }()
    
    private func daysSectionLayout() -> NSCollectionLayoutSection{
        let daySectionSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(60))
        let daySectionLayout = NSCollectionLayoutBoundarySupplementaryItem( layoutSize: daySectionSize,  elementKind: DayWeatherCollectionReusableView.kind, alignment: .top)
        daySectionLayout.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 24, bottom: 8, trailing: 8)
        let itemSize = NSCollectionLayoutSize(  widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize( widthDimension: .absolute(120), heightDimension: .absolute(140))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitem: item, count: 1)
        group.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 24, bottom: 16, trailing: 24)
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuous
        section.boundarySupplementaryItems = [
            daySectionLayout
        ]
        return section
    }
    
    private func mainSectionLayout() -> NSCollectionLayoutSection{
        
        
        let itemSize = NSCollectionLayoutSize(  widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize( widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(250))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitem: item, count: 1)
        group.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 16, trailing: 8)
        let section = NSCollectionLayoutSection(group: group)
        return section
    }
    
    func applySnapshot(weather:CityWeatherModel) {
        
        var snapshot = NSDiffableDataSourceSnapshot<CityWeatherInfoDataSource.Section,HourWeatherModel>()
        snapshot.appendSections([.main])
        snapshot.appendItems([weather.currentWeather], toSection: .main)
        snapshot.appendSections(weather.days.map({CityWeatherInfoDataSource.Section.day(data: $0)}))
        weather.days.forEach({
            snapshot.appendItems($0.hours, toSection: .day(data: $0))
        })
        
        dataSource.apply(snapshot, animatingDifferences: false)
        
    }
    
}
