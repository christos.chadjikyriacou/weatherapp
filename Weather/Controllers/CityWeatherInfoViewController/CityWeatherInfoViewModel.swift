//
//  CityWeatherInfoViewModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 10/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import Combine

protocol CityWeatherInfoViewModel:RequestViewModel {
    var weatherSubject:PassthroughSubject<CityWeatherModel,Never>{get}
    func getWeather(city:CityModel)
}

class CityWeatherInfoViewModelImpl:RequestViewModelImpl,CityWeatherInfoViewModel {
    
    @Inject
    private var weatherRepository:WeatherRepository
    
    
    var weatherSubject = PassthroughSubject<CityWeatherModel,Never>()
    
    func getWeather(city:CityModel) {
        isLoadingSubject.send(true)
        weatherRepository.getWather(for: city, onCompletionTemp: { (cityWeather) in
            weatherSubject.send(cityWeather)
        }) { (result) in
            self.isLoadingSubject.send(false)
            switch result {
            case .failure(let error):
                self.errorSubject.send(error)
            case .success(let response):
                self.weatherSubject.send(response.data)
            }
        }
    }
}
