//
//  EditCitiesNavigationController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 09/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class EditCitiesNavigationController:BaseNavigationController {
    
    
    init(onDismiss:@escaping ()->Void) {
        super.init(nibName: nil, bundle: nil)
        setViewControllers([EditCitiesViewController(onDismissed: onDismiss)], animated: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
