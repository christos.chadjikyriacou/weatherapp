//
//  MainNavigationController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


class MainNavigationController:BaseNavigationController {
    
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setViewControllers([WeatherInfoPageViewController()], animated: false)
    }
    
    
   
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
