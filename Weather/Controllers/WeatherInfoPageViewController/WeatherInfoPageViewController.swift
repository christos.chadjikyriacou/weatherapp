//
//  WeatherInfoPageViewController.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 07/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

class WeatherInfoPageViewController: BaseViewController {
    
    fileprivate enum PageType {
        case next
        case previous
    }
    
    @Inject
    private var viewModel:WeatherInfoPageViewModel
    
    
    @IBOutlet private weak var pageControllerContainerStackView: UIStackView!
    @IBOutlet private weak var pageControl: UIPageControl! {
        willSet {
            
            newValue.currentPageIndicatorTintColor = Asset.Colors.white.color
            newValue.pageIndicatorTintColor = Asset.Colors.gray.color
            
        }
    }
    @IBOutlet private weak var addCityView: AddCityView!
    @IBOutlet private weak var pageControllerContainer: UIView! {
        willSet {
            newValue.addSubviewAndFillParent(view:  pageController.view)
        }
    }
    
    override var navigationController: MainNavigationController? {
        return super.navigationController as? MainNavigationController
    }
    
    
    
    
    
    private lazy var pageController:UIPageViewController = {
        let controller = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options:nil)
        controller.dataSource = self
        controller.delegate = self
        return controller
    }()
    
    
    private lazy var editItem:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(edit))
    
    @objc private func edit() {
        navigationController?.editCities(onDismiss:  viewModel.getSelectedCities)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
      
        viewModel.getSelectedCities()
    }
    
    
    override func configureObservers() {
        super.configureObservers()
        addCityView.onAddCityPressed.sink { (button) in
            self.navigationController?.addCity(onDismiss: self.viewModel.getSelectedCities)
        }
        .store(in: &cancellables)
        
        viewModel.selectedCitiesSubject
            .assign(to: \.cities, on: self)
            .store(in: &cancellables)
        
    }
    
    
    var cities:[CityModel] = [] {
        willSet {
            configure(cities: newValue)
        }
    }
    
    
    
    private func configure(cities:[CityModel]) {
        
        guard let firstCity = cities.first else {
            navigationItem.title = L10n.noCity
            navigationItem.setLeftBarButton(nil, animated: true)
            navigationItem.setRightBarButton(nil, animated: true)
            addCityView.isHidden = false
            pageControllerContainer.isHidden = true
            return
        }
        
        
        navigationItem.title = firstCity.areaName
        navigationItem.setRightBarButton(editItem, animated: true)
        
        let firstController = CityWeatherInfoViewController(city: firstCity)
        pageController.setViewControllers([firstController], direction: .forward, animated: false)
        pageControl.numberOfPages = cities.count
        
        
        addCityView.isHidden = true
        pageControllerContainer.isHidden = false
    }
    
    
    
    
    fileprivate func index(of currentController:UIViewController) -> Int? {
        guard let currentCity = (currentController as? CityWeatherInfoViewController)?.city else { return nil }
        guard let currentCityIndex = cities.firstIndex(of: currentCity) else { return nil}
        return currentCityIndex
    }
    
    
    fileprivate func controller(operation:(Int,Int)->Int,currentController:UIViewController) -> CityWeatherInfoViewController? {
        guard let currentCityIndex = index(of: currentController) else { return nil}
        guard let nextCity = cities[safe: operation(currentCityIndex,1)] else { return nil}
        return CityWeatherInfoViewController(city: nextCity)
    }
    
}


extension WeatherInfoPageViewController:UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        return controller(operation:+, currentController: viewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        return controller(operation: -, currentController: viewController)
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else { return }
        guard let currentController = (pageViewController.viewControllers?.first as? CityWeatherInfoViewController) else { return }
        pageControl.currentPage = index(of: currentController) ?? 0
        navigationItem.title = currentController.city.areaName
    }
    
    
    
}
