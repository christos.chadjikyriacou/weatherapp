//
//  WeatherInfoPageViewModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import Combine

protocol WeatherInfoPageViewModel:RequestViewModel {
    func getSelectedCities()
    var selectedCitiesSubject:PassthroughSubject<[CityModel],Never>{get}
    
}


class WeatherInfoPageViewModelImpl:RequestViewModelImpl,WeatherInfoPageViewModel {
    
    
    
    @Inject
    var citiesRepository:CitiesRepository
    
    var selectedCitiesSubject = PassthroughSubject<[CityModel],Never>()
        
    
    func getSelectedCities() {
        selectedCitiesSubject.send(citiesRepository.allSelectedCities)
        
    }
    
    
}


