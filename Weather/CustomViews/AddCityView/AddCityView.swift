//
//  AddCityView.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit
import Combine

class AddCityView:XibView {
    @IBOutlet weak private var addCityImageView: UIImageView!
    @IBOutlet weak private var addCityButton: UIButton! {
        willSet {
            newValue.backgroundColor = UIColor.systemBlue
            newValue.setTitle(L10n.addCity, for: .normal)
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addCityButton.clipsToBounds = true
        addCityButton.layer.cornerRadius = addCityButton.frame.height / 2
    }

    
        
    
    public lazy var onAddCityPressed = addCityButton.publisher(for: .touchUpInside).eraseToAnyPublisher()
    

    
}
