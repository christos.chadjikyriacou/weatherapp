//
//  CodingUserInfoKey + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation

public extension CodingUserInfoKey {

    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")
}
