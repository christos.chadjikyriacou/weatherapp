//
//  Date + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


extension Date {
    var isToday:Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    var isTomorrow:Bool {
        return Calendar.current.isDateInTomorrow(self)
    }
    
    var dayOfWeek:String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized(with: .current)
    }
}
