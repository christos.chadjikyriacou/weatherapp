//
//  JSONDecoder + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import CoreData

extension JSONDecoder  {
    
    var managedObjectContext:NSManagedObjectContext? {
        set {
            guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {  return  }
            userInfo[codingUserInfoKeyManagedObjectContext] = newValue
        }
        
        get {
            guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {  return nil }
            return userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext
        }
    }
}

