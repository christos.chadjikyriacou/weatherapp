//
//  Keypath + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 09/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


extension KeyPath where Root: NSObject {
    var toString: String {
        return NSExpression(forKeyPath: self).keyPath
    }
}
