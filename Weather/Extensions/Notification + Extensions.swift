//
//  Notification + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

extension Notification {
    public var keyboardHeight:CGFloat {
        return ((userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.size.height ?? 0)
    }
    
    
    public var keyboardAnimation:TimeInterval {
        return (userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval) ?? 0
    }
}
