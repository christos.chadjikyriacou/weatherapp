//
//  NotificationCenter + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation

extension NotificationCenter:HasDefaultIplementation {
    
    
    public static func defaultIplmentation() -> NotificationCenter {
         return NotificationCenter.default
    }
}
