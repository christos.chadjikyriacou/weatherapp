//
//  String + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 07/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


extension String {
    
    enum DataError:String,Error {
        case invalidURL = "Invalid URL"
        case invalidDateFormat = "Invalid Date Format"
        case invalidBoolFormat = "Invalid Bool Format"
    }
    
    var toURL:URL? {
        return URL(string: self)
    }
    
    func toURLOrThrow() throws  ->  URL  {
        
        guard let url = self.toURL else {
            throw DataError.invalidURL
        }
        
        return url
    }
    
    func toBool(trueValue:String,falseValue:String) -> Bool? {
        if self == trueValue {
            return true
        }
        
        if self == falseValue {
            return false
        }
        
        return nil
    }
    
    
    func toBoolOrThrow(trueValue:String,falseValue:String) throws -> Bool {
        guard let boolValue = self.toBool(trueValue: trueValue, falseValue: falseValue) else {
            throw DataError.invalidBoolFormat
        }
        
        return boolValue
    }
    

    
    var toDouble:Double? {
        return Double(self)
    }
    
    func toDate(withFormat format:String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.date(from: self)
    }
    
    func toDateOrThrow(withFormat format:String) throws -> Date {
        guard let date = self.toDate(withFormat: format) else {
            throw DataError.invalidDateFormat
        }
        
        return date
    }
    
    var toCelciusString:String {
        return self + "°"
    }
}
