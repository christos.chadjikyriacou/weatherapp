//
//  UITableViewCell + Extensions.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

public extension UITableViewCell {
    
    var tableView:UITableView? {
        return superview as? UITableView
    }
    
    var indexInSection:Int? {
        guard let indexPath = tableView?.indexPathForRow(at: center) else {return nil}
        return indexPath.row
    }
    
    var isFirst:Bool {

        guard tableView?.tableHeaderView != self else { return true }
        
        guard let indexPath = tableView?.indexPathForRow(at: center) else {return false}
        return indexPath.row == 0
       
    }
    
    var isLastInSection:Bool {

        guard let indexPath = tableView?.indexPathForRow(at: center) else {return false}
        guard let totalRows = tableView?.numberOfRows(inSection: indexPath.section) else { return false}
        return indexPath.row == totalRows - 1
    }
    
    var isLast:Bool {

        guard let indexPath = tableView?.indexPathForRow(at: center) else {return false}
        guard let totalSections = tableView?.numberOfSections else { return false}
        guard let totalRows = tableView?.numberOfRows(inSection: indexPath.section) else { return false}
        return indexPath.section == totalSections - 1 &&  indexPath.row == totalRows - 1
    }
    
    

    
    static func create(tableView:UITableView,reuseIdentifier:String? = nil) -> Self {
        return create(tableView: tableView, reuseIdentifier: (reuseIdentifier ?? String(describing:self)), type: self)
    }
    
    fileprivate static func create<T : UITableViewCell>(tableView:UITableView,reuseIdentifier:String,type: T.Type) -> T {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? T else {
            let nib = UINib(nibName: reuseIdentifier, bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
            return create(tableView: tableView,reuseIdentifier:reuseIdentifier, type: type)
        }
        
        return cell

    }
    
    
    

}
