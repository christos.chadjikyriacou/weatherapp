// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {
  /// Add City
  internal static let addCity = L10n.tr("Localizable", "addCity")
  /// Edit Cities
  internal static let editCities = L10n.tr("Localizable", "editCities")
  /// Error
  internal static let error = L10n.tr("Localizable", "error")
  /// No City
  internal static let noCity = L10n.tr("Localizable", "noCity")
  /// OK
  internal static let ok = L10n.tr("Localizable", "ok")
  /// Today
  internal static let today = L10n.tr("Localizable", "today")
  /// Tomorrow
  internal static let tomorrow = L10n.tr("Localizable", "tomorrow")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    Bundle(for: BundleToken.self)
  }()
}
// swiftlint:enable convenience_type
