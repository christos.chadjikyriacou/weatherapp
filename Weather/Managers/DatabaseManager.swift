//
//  DatabaseManager.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//


import UIKit
import CoreData

class DatabaseManager {
    
    
    
    static let shared = DatabaseManager()
    
    
    func start() {
        NotificationCenter.default.addObserver(forName: UIApplication.willTerminateNotification, object: self, queue: nil) { (_) in
            self.saveContext()
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.NSManagedObjectContextDidSave, object: self, queue: nil) { [weak self] (saveNotification) -> Void in
            self?.saveContext()
        }
    }
  
    
     var context:NSManagedObjectContext {
        return Thread.isMainThread ? persistentContainer.viewContext : privateContext
    }
    
 
    
    lazy var privateContext:NSManagedObjectContext = {
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = self.persistentContainer.viewContext
        return context
    }()
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Weather")
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
             try? context.save()
        }
    }
}


extension DatabaseManager:Starter {
    static func start() {
        shared.start()
    }
}


extension DatabaseManager:HasDefaultIplementation {
    static func defaultIplmentation() -> DatabaseManager {
        return DatabaseManager.shared
    }
}
