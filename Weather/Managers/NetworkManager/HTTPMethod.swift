//
//  HTTPMethod.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation

enum HTTPMethod:String {
      case post = "POST"
      case get = "GET"
      case put = "PUT"
      case patch = "PATCH"
      case delete = "DELETE"
  }
  
