//
//  NetworkErrors.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


enum NetworkError:String, Error {

    case unknownError = "Unknown Error"
    case noDataReceived = "No Data Received"
    case invalidResponse = "Invalid Reposne"
    
}



