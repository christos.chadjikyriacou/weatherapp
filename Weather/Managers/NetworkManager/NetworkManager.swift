//
//  NetworkManager.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit
import Combine


typealias NetworkResponseCompletionHandler<ResponseObject:Decodable> = (Result<NetworkManager.Response<ResponseObject>,Error>)->Void

class NetworkManager:NSObject {
    
    
    static let shared = NetworkManager()
    
    struct Request {
        let url:URL
        let httpMethod:HTTPMethod
        
        var urlRequest:URLRequest  {
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = httpMethod.rawValue
            return urlRequest
        }
    }
    
    struct Response<ResponseObject:Decodable> {
        let data:ResponseObject
        let response: URLResponse?
    }
    
    private lazy var session = URLSession(configuration: .default, delegate: SecurityManager.shared, delegateQueue: nil)
    
    
    func run<ResponseObject: Decodable>(_ request:Request,
                                        onCompletion:@escaping NetworkResponseCompletionHandler<ResponseObject>,
                                        onObjectReceivedSuccess:((ResponseObject)->Void)? = nil){
        
        
        
        session.dataTask(with: request.urlRequest) { (data, response, error) in
            DispatchQueue.main.async {
                guard let httpResponse = response as? HTTPURLResponse else {
                    onCompletion(.failure(NetworkError.unknownError))
                    return
                }
                
                guard 200..<300 ~= httpResponse.statusCode else {
                    onCompletion(.failure(HTTPStatusCode(rawValue: httpResponse.statusCode) ?? NetworkError.unknownError))
                    return
                }
                
                guard let data = data else {
                    onCompletion(.failure(NetworkError.noDataReceived))
                    return
                }
                
                do {
                    let data = try JSONDecoder().decode(ResponseObject.self, from: data)
                    onObjectReceivedSuccess?(data)
                    onCompletion(.success(Response(data: data, response: response)))
                }
                catch(let error) {
                    onCompletion(.failure(error))
                }
            }
            
            
            
        }.resume()
        
    }
    
    
}

extension NetworkManager:HasDefaultIplementation {
    static func defaultIplmentation() -> NetworkManager {
        return NetworkManager.shared
    }
}

extension NetworkManager.Response where ResponseObject:ExpressibleByNilLiteral {
    
    init(response: URLResponse) {
        self.init(data:nil, response:response)
    }
}

extension NetworkManager.Request {
    
    init(urlString:String,httpMethod:HTTPMethod) throws {
      
        let url = try urlString.toURLOrThrow()
        
        self.init(url: url, httpMethod: httpMethod)
    }
}





