//
//  SecurityManager.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import TrustKit


class SecurityManager:NSObject,HasDefaultIplementation {
    
    private(set) var configurations:SecurityManagerConfigurations?
    
    static func defaultIplmentation() -> SecurityManager {
        return shared
    }
    
    static let shared = SecurityManager()
    
    func setConfiguration(configurations:SecurityManagerConfigurations) {
        self.configurations = configurations
        
        
    }
    
    var certificateValidator:TSKPinningValidator?  {
        guard let configurations = self.configurations else { return nil}
        
        switch configurations.certificatePinningStatus {
        case .enabled(let endpoint,let publicKeys):
            guard let domain = URL(string:endpoint)?.host else {
                
                return nil
            }
            
            
            let trustKitConfig = [
                kTSKPinnedDomains: [
                    domain: [
                        kTSKIncludeSubdomains : true,
                        kTSKEnforcePinning: true,
                        kTSKPublicKeyHashes: publicKeys,]]] as [String : Any]
            
            return  TrustKit.init(configuration: trustKitConfig).pinningValidator
        default:
             return nil
        }

    }
}


extension SecurityManager:URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate  {
    
    
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let validator = certificateValidator else {
            completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil);
            return
        }
        
        if !validator.handle(challenge, completionHandler: completionHandler) {
            completionHandler(URLSession.AuthChallengeDisposition.performDefaultHandling, nil);
        }
        
    }
}
