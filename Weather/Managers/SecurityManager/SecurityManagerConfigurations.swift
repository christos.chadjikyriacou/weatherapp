//
//  SecurityManagerConfigurations.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
class SecurityManagerConfigurations {
    enum CertificatePinningStatus {
        case enabled(endpoint:String,publicKeys:[String])
        case disable
    }
    
    let certificatePinningStatus:CertificatePinningStatus
    
    
    init(certificatePinningStatus:CertificatePinningStatus) {
        
        self.certificatePinningStatus = certificatePinningStatus
    }
}
