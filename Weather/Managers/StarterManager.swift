//
//  StarterManager.swift
//  NettellerComponents
//
//  Created by Christos Chadjikyriacou on 21/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//

import UIKit


public class StarterManager {
    
  

    
    public static func with(starters:[Starter.Type]) {
         starters.forEach { (starter) in
                starter.start()
         }
    }
    
}
