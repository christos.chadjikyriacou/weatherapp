//
//  TargetManager.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation

class TargetManager:HasDefaultIplementation {
    
    public static func defaultIplmentation() -> TargetManager {
        return TargetManager.shared
    }

    public static let shared = TargetManager()
    
    public enum Target {
        case release
        case uat
        case debug
        case qa
    }
    
    public var current:Target {
        #if DEBUG
        return .debug
        #elseif UAT
        return .uat
        #elseif QA
        return .qa
        #else
        return .release
        #endif
    }
    
    
}
