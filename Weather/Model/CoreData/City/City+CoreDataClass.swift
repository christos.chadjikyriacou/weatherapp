//
//  City+CoreDataClass.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//
//

import Foundation
import CoreData

@objc(City)
public class City: NSManagedObject {


    func apply(city:CityModel) {
        self.areaName = city.areaName
        self.country = city.country
        self.latitude = city.latitude
        self.longitude = city.longitude
        self.region = city.region
        self.weatherURL = city.weatherURL
    }
}


