//
//  City+CoreDataProperties.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//
//

import Foundation
import CoreData


extension City {

    static let entityName = String(describing: City.self)
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: entityName)
    }

    @NSManaged public var areaName: String
    @NSManaged public var region: String
    @NSManaged public var latitude: String
    @NSManaged public var longitude: String
    @NSManaged public var weatherURL: URL
    @NSManaged public var country: String

}
