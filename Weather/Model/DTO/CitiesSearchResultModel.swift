//
//  SearchResultDao.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 07/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


struct CitiesSearchResultModel:Decodable {
    
    enum CodingKeys: String, CodingKey {
        case search_api
        case result
    }
    
    let cities:[CityModel]
    
    
    init(from decoder: Decoder) throws {
        cities = (try? decoder
            .container(keyedBy: CodingKeys.self)
            .nestedContainer(keyedBy: CodingKeys.self, forKey: .search_api)
            .decode([CityModel].self, forKey: .result)) ?? []
    

    }
    
    
    
}
