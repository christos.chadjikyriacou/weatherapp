//
//  CityDao.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 07/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


struct CityModel:Decodable,Hashable {
    
    
    enum CodingKeys: String, CodingKey {
        case value
        case areaName
        case region
        case latitude
        case longitude
        case weatherUrl
        case country
    }
    
    let uuid = UUID()
    let longitude:String
    let latitude:String
    let areaName: String
    let region: String
    let weatherURL: URL
    let country:String
    
    
    var fullName:String  {
 
        guard self.areaName == self.region || self.region.isEmpty else {
            return self.areaName + ", " + self.region + ", " + self.country
        }
        return self.areaName + ", " + self.country
    }
    
    
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.region = try container.decodeStringArrayValue(key: .region)
        self.areaName = try container.decodeStringArrayValue(key: .areaName)
        self.weatherURL = try container.decodeStringArrayValue(key: .weatherUrl).toURLOrThrow()
        self.country = try container.decodeStringArrayValue(key: .country)
        self.latitude = try container.decode(String.self, forKey: .latitude)
        self.longitude = try container.decode(String.self, forKey: .longitude)
    }
    
    
    init(city:City) {
        self.latitude = city.latitude
        self.longitude = city.longitude
        self.areaName = city.areaName
        self.region = city.region
        self.weatherURL = city.weatherURL
        self.country = city.country
    }
    
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(uuid)

    }

    static func == (lhs: CityModel, rhs: CityModel) -> Bool {
        return lhs.uuid == rhs.uuid
    }

    
    
}



extension Array where Element == City {
    
    var toCityDao:[CityModel] {
        return self.map({CityModel(city: $0)})
    }
}
