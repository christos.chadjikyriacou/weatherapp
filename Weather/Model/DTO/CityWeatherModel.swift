//
//  CityResultModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 10/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


struct CityWeatherModel:Decodable {
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    enum WeatherCodingKeys: String, CodingKey {
          case current_condition
          case weather
      }
    
    let currentWeather:HourWeatherModel
    let days:[DayWeatherModel]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let weatherContainer = try container.nestedContainer(keyedBy: WeatherCodingKeys.self, forKey: .data)
       
        currentWeather = try weatherContainer.decodeHourWeatherModelArrayValue(key:  .current_condition)
        days = try  weatherContainer.decodeIfPresent([DayWeatherModel].self,forKey: .weather) ?? []
    }
    
}
