//
//  DayWeatherModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 10/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


struct DayWeatherModel:Decodable,Hashable {
    
    enum CodingKeys: String, CodingKey {
        case date
        case avgtempC
        case mintempC
        case maxtempC
        case hourly
    }
    
    let date:Date
    let avgTemp:String
    let minTemp:String
    let maxTemp:String
    let hours:[HourWeatherModel]
    
    
   
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.date = try container.decode(String.self, forKey: .date).toDateOrThrow(withFormat: "yyyy-MM-dd")
        self.avgTemp = try container.decode(String.self, forKey: .avgtempC)
        self.minTemp = try container.decode(String.self, forKey: .mintempC)
        self.maxTemp = try container.decode(String.self, forKey: .maxtempC)
        self.hours = try container.decodeIfPresent([HourWeatherModel].self, forKey: .hourly) ?? []
    }
    
    
    func hash(into hasher: inout Hasher) {
         hasher.combine(date)
     }

     static func == (lhs: DayWeatherModel, rhs: DayWeatherModel) -> Bool {
         return lhs.date == rhs.date
     }
}
