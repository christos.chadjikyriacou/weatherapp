//
//  HourWeatherModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 10/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


struct HourWeatherModel:Decodable,Hashable {
    private let uuid = UUID()
    enum CodingKeys: String, CodingKey {
        case value
        case observation_time
        case time
        case temp_C , tempC
        case weatherCode
        case weatherDesc
        case isdaytime
        case weatherIconUrl
    }
    
    let observationTime:String?
    let time:String?
    let temp:String
    let code:String
    let desc:String
    let icon:URL
    let isDayTime:Bool
    let weatherType:WeatherTypeModel?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
     
        self.desc = try container.decodeStringArrayValue(key: .weatherDesc)
        self.icon = try container.decodeStringArrayValue(key: .weatherIconUrl).replacingOccurrences(of: "http://", with: "https://").toURLOrThrow()
        self.code = try container.decode(String.self, forKey: .weatherCode)
        self.observationTime = try container.decodeIfPresent(String.self, forKey: .observation_time)
        self.time = try container.decodeIfPresent(String.self, forKey: .time)
        self.temp = try container.decodeIfPresent(String.self, forKey: .temp_C) ?? container.decode(String.self, forKey: .tempC)
        self.isDayTime = try container.decode(String.self, forKey: .isdaytime).toBoolOrThrow(trueValue: "yes", falseValue: "no")
        self.weatherType = try code.toWeatherTypeOrThrow()
    }
    
}

extension KeyedDecodingContainer  {
    func decodeHourWeatherModelArrayValue(key:Self.Key) throws -> HourWeatherModel {
        guard let value = try self.decode([HourWeatherModel].self, forKey: key).first else {
            throw DecodingError.dataCorruptedError(forKey: key, in: self, debugDescription: "")
        }
        
        return value
        
    }
}
