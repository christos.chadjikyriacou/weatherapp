//
//  StringValue.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 07/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


struct StringArrayValue:Codable {
    let value:String
}

extension KeyedDecodingContainer  {
    
    func decodeStringValue(key:Self.Key) throws -> String {
        guard let value = try self.decode([StringArrayValue].self, forKey: key).first?.value else {
            throw DecodingError.dataCorruptedError(forKey: key, in: self, debugDescription: "")
        }
        
        return value
        
    }
}
