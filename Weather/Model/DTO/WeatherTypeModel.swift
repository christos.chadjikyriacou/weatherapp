//
//  WeatherTypeModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 10/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import UIKit

/*
 
 395    Moderate or heavy snow in area with thunder    wsymbol_0012_heavy_snow_showers    wsymbol_0028_heavy_snow_showers_night
 392    Patchy light snow in area with thunder    wsymbol_0016_thundery_showers    wsymbol_0032_thundery_showers_night
 389    Moderate or heavy rain in area with thunder    wsymbol_0024_thunderstorms    wsymbol_0040_thunderstorms_night
 386    Patchy light rain in area with thunder    wsymbol_0016_thundery_showers    wsymbol_0032_thundery_showers_night
 377    Moderate or heavy showers of ice pellets    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 374    Light showers of ice pellets    wsymbol_0013_sleet_showers    wsymbol_0029_sleet_showers_night
 371    Moderate or heavy snow showers    wsymbol_0012_heavy_snow_showers    wsymbol_0028_heavy_snow_showers_night
 368    Light snow showers    wsymbol_0011_light_snow_showers    wsymbol_0027_light_snow_showers_night
 365    Moderate or heavy sleet showers    wsymbol_0013_sleet_showers    wsymbol_0029_sleet_showers_night
 362    Light sleet showers    wsymbol_0013_sleet_showers    wsymbol_0029_sleet_showers_night
 359    Torrential rain shower    wsymbol_0018_cloudy_with_heavy_rain    wsymbol_0034_cloudy_with_heavy_rain_night
 356    Moderate or heavy rain shower    wsymbol_0010_heavy_rain_showers    wsymbol_0026_heavy_rain_showers_night
 353    Light rain shower    wsymbol_0009_light_rain_showers    wsymbol_0025_light_rain_showers_night
 350    Ice pellets    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 338    Heavy snow    wsymbol_0020_cloudy_with_heavy_snow    wsymbol_0036_cloudy_with_heavy_snow_night
 335    Patchy heavy snow    wsymbol_0012_heavy_snow_showers    wsymbol_0028_heavy_snow_showers_night
 332    Moderate snow    wsymbol_0020_cloudy_with_heavy_snow    wsymbol_0036_cloudy_with_heavy_snow_night
 329    Patchy moderate snow    wsymbol_0020_cloudy_with_heavy_snow    wsymbol_0036_cloudy_with_heavy_snow_night
 326    Light snow    wsymbol_0011_light_snow_showers    wsymbol_0027_light_snow_showers_night
 323    Patchy light snow    wsymbol_0011_light_snow_showers    wsymbol_0027_light_snow_showers_night
 320    Moderate or heavy sleet    wsymbol_0019_cloudy_with_light_snow    wsymbol_0035_cloudy_with_light_snow_night
 317    Light sleet    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 314    Moderate or Heavy freezing rain    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 311    Light freezing rain    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 308    Heavy rain    wsymbol_0018_cloudy_with_heavy_rain    wsymbol_0034_cloudy_with_heavy_rain_night
 305    Heavy rain at times    wsymbol_0010_heavy_rain_showers    wsymbol_0026_heavy_rain_showers_night
 302    Moderate rain    wsymbol_0018_cloudy_with_heavy_rain    wsymbol_0034_cloudy_with_heavy_rain_night
 299    Moderate rain at times    wsymbol_0010_heavy_rain_showers    wsymbol_0026_heavy_rain_showers_night
 296    Light rain    wsymbol_0017_cloudy_with_light_rain    wsymbol_0025_light_rain_showers_night
 293    Patchy light rain    wsymbol_0017_cloudy_with_light_rain    wsymbol_0033_cloudy_with_light_rain_night
 284    Heavy freezing drizzle    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 281    Freezing drizzle    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 266    Light drizzle    wsymbol_0017_cloudy_with_light_rain    wsymbol_0033_cloudy_with_light_rain_night
 263    Patchy light drizzle    wsymbol_0009_light_rain_showers    wsymbol_0025_light_rain_showers_night
 260    Freezing fog    wsymbol_0007_fog    wsymbol_0007_fog
 248    Fog    wsymbol_0007_fog    wsymbol_0007_fog
 230    Blizzard    wsymbol_0020_cloudy_with_heavy_snow    wsymbol_0036_cloudy_with_heavy_snow_night
 227    Blowing snow    wsymbol_0019_cloudy_with_light_snow    wsymbol_0035_cloudy_with_light_snow_night
 200    Thundery outbreaks in nearby    wsymbol_0016_thundery_showers    wsymbol_0032_thundery_showers_night
 185    Patchy freezing drizzle nearby    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 182    Patchy sleet nearby    wsymbol_0021_cloudy_with_sleet    wsymbol_0037_cloudy_with_sleet_night
 179    Patchy snow nearby    wsymbol_0013_sleet_showers    wsymbol_0029_sleet_showers_night
 176    Patchy rain nearby    wsymbol_0009_light_rain_showers    wsymbol_0025_light_rain_showers_night
 143    Mist    wsymbol_0006_mist    wsymbol_0006_mist
 122    Overcast    wsymbol_0004_black_low_cloud    wsymbol_0004_black_low_cloud
 119    Cloudy    wsymbol_0003_white_cloud    wsymbol_0004_black_low_cloud
 116    Partly Cloudy    wsymbol_0002_sunny_intervals    wsymbol_0008_clear_sky_night
 113    Clear/Sunny    wsymbol_0001_sunny    wsymbol_0008_clear_sky_night
 */

enum WeatherTypeModel {
    case snow
    case clear
    case rain
    case heavyRain
    case cloudy
    case thunder
    case fog
    case sleet
    
    func image(isDayTime:Bool) -> UIImage {
        switch self {
        case .clear:
            return isDayTime ? Asset.Images.clearDay.image : Asset.Images.clearNight.image
        case .cloudy:
            return  isDayTime ? Asset.Images.cloudyDay.image : Asset.Images.cloudyNight.image
        case .fog:
            return isDayTime ? Asset.Images.fogDay.image : Asset.Images.fogNight.image
        case .heavyRain:
            return isDayTime ? Asset.Images.heavyRainDay.image : Asset.Images.heavyRainNight.image
        case .rain:
            return isDayTime ? Asset.Images.rainDay.image : Asset.Images.rainNight.image
        case .thunder:
            return isDayTime ? Asset.Images.stormDay.image : Asset.Images.stormNight.image
        case .sleet:
            return isDayTime ? Asset.Images.sleetDay.image : Asset.Images.sleetNight.image
        case .snow:
            return isDayTime ? Asset.Images.snowDay.image : Asset.Images.snowNight.image
        }
    }
    
    init?(code:String) {
        switch code {
        case "113":
            self = .clear
        case "116","119","122":
            self = .cloudy
        case "143","248","260":
            self = .fog
        case "200":
            self = .thunder
        case "179","182","317","320","374","377":
            self = .sleet
        case "227","230","323","326","329","332","335","338","350","392","395":
            self = .snow
        case "185","176","263","266","281","284","293","296","299","302","311","353","359","362","365","368","371","386":
            self = .rain
        case "305","308","314","356","389":
            self = .heavyRain
        default:
            return nil
        }
    }
    
    
}

extension String {
    
    enum WeatherTypeError:String,Error {

        case invalidWeatherType = "Invalid Weather Type"
    }
    
    var toWeatherType:WeatherTypeModel? {
        return WeatherTypeModel(code: self)
    }
    
    func toWeatherTypeOrThrow() throws -> WeatherTypeModel {
        guard let weatherType = self.toWeatherType else {
            throw WeatherTypeError.invalidWeatherType
        }
        
        return weatherType
    }
}
