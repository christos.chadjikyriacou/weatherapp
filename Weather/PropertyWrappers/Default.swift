//
//  Default.swift
//  NettellerComponents
//
//  Created by Christos Chadjikyriacou on 22/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//

import Foundation


@propertyWrapper
public struct Default<T:HasDefaultIplementation> {

    public init() {
        
    }


    public var wrappedValue:T {
        mutating get {
            return T.defaultIplmentation() as! T
        }

    }
}

