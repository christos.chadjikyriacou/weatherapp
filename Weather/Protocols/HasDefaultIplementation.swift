//
//  HasDefaultIplementation.swift
//  NettellerComponents
//
//  Created by Christos Chadjikyriacou on 22/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//

import Foundation

public protocol HasDefaultIplementation {
    associatedtype T
    static func defaultIplmentation() -> T

}
