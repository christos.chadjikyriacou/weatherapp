//
//  CityRepository.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 06/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import Combine
import CoreData

protocol CitiesRepository {
    func search(query:String,onCompletion:@escaping NetworkResponseCompletionHandler<CitiesSearchResultModel>)
    func select(city:CityModel)
    func unselect(city:CityModel) throws
    
    var allSelectedCities:[CityModel] {get}
    var selectedCitiesCount:Int {get}
    
}

class CitiesRepositoryImpl:CitiesRepository {
    
    @Default
    var client:NetworkManager
    
    @Default
    var database:DatabaseManager
    
    func search(query:String,onCompletion:@escaping NetworkResponseCompletionHandler<CitiesSearchResultModel>)  {
        let endpoint =  Endpoints.search(query: query)
        do {
            let request = try NetworkManager.Request(urlString: endpoint, httpMethod: .get)
            client.run(request, onCompletion: onCompletion)
        }
        catch(let error) {
            onCompletion(.failure(error))
        }
        
    }
    
    var allSelectedCities:[CityModel] {
        let request = NSFetchRequest<City>.init(entityName: City.entityName)
        return ((try? database.context.fetch(request)) ?? []).toCityDao
        
    }
    
    var selectedCitiesCount:Int {
        let request = NSFetchRequest<City>.init(entityName: City.entityName)
        return (try? database.context.count(for: request)) ?? 0
    }
    
    
    func unselect(city:CityModel) throws {
        let context = database.context
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: City.entityName).apply(closure: {
            let areaNamePredicate = NSPredicate(format: "\((\City.areaName).toString) = %@", city.areaName)
            let regionPredicate = NSPredicate(format: "\((\City.region).toString) = %@", city.region)
            let countryPredicate = NSPredicate(format: "\((\City.country).toString) = %@", city.country)
            $0.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [
                areaNamePredicate,
                regionPredicate,
                countryPredicate
            ])
        })
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        try context.execute(deleteRequest)
        database.saveContext()
    }
    
    func select(city:CityModel) {
        let context = database.context
        let newCity = NSEntityDescription.insertNewObject(forEntityName: City.entityName, into: context) as? City
        newCity?.apply(city: city)
        database.saveContext()
    }
}


