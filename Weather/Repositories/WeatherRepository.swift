//
//  WeatherRepository.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 10/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import Combine

protocol WeatherRepository {
    func getWather(for city:CityModel,onCompletionTemp:(CityWeatherModel)->Void,onCompletion:@escaping NetworkResponseCompletionHandler<CityWeatherModel>)
}

class WeatherRepositoryImpl:WeatherRepository {

    
    
    @Default
    var client:NetworkManager
    
    private var temporaryWeatherData:[CityModel:CityWeatherModel] = [:]

    
    func getWather(for city:CityModel,onCompletionTemp:(CityWeatherModel)->Void,onCompletion:@escaping NetworkResponseCompletionHandler<CityWeatherModel>) {
        
        if let temporaryStoredWeatherData =  temporaryWeatherData[city] {
            onCompletionTemp(temporaryStoredWeatherData)
        }
        
        do {
            let endpoint = Endpoints.weather(location: city)
            print(endpoint)
            let request = try NetworkManager.Request(urlString: endpoint, httpMethod: .get)
            client.run(request, onCompletion: onCompletion) { (recievedData) in
                self.temporaryWeatherData[city]  = recievedData
                
            }
            
        }
        catch(let error) {
            onCompletion(.failure(error))
        }
        
    }
    
}
