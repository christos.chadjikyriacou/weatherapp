//
//  DependencyManager + Starter.swift
//  VTB Armenia
//
//  Created by Christos Chadjikyriacou on 21/03/2020.
//  Copyright © 2020 NetInfo plc. All rights reserved.
//          

import Foundation

class DependenciesResolver:Starter {
    
    @Default
    private static var container:DependeciesContainer
    

    public static func start() {
        registerRepositories()
        registerViewModel()
        
    }
    
    private static func registerRepositories() {
        container.register(serviceType: CitiesRepository.self, resolve: { CitiesRepositoryImpl() })
        container.register(serviceType: WeatherRepository.self, resolve: { WeatherRepositoryImpl() })
    }
    
    
    private  static func registerViewModel() {
        container.register(serviceType: EditCitiesViewModel.self, resolve: {EditCitiesViewModelImpl()})
        container.register(serviceType: AddCitiesViewModel.self, resolve: {AddCitiesViewModelImpl()})
        container.register(serviceType: WeatherInfoPageViewModel.self, resolve: {WeatherInfoPageViewModelImpl()})
        container.register(serviceType: CityWeatherInfoViewModel.self, resolve:{CityWeatherInfoViewModelImpl()})
    }
    
    
}




