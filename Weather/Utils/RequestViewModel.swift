//
//  RequestViewModel.swift
//  Weather
//
//  Created by Christos Chadjikyriacou on 08/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation
import Combine

protocol RequestViewModel {
    var errorSubject:PassthroughSubject<Error,Never>{get}
     var isLoadingSubject:CurrentValueSubject<Bool,Never> {get}
}

class RequestViewModelImpl:RequestViewModel{
    var errorSubject = PassthroughSubject<Error, Never>()
    var isLoadingSubject = CurrentValueSubject<Bool,Never>(false)
}
