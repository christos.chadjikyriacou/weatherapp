//
//  ExtensionTests.swift
//  WeatherTests
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import XCTest
@testable import Weather

class ExtensionsTests:XCTestCase {
    
    func testStirngToBool()  {
        let testTrueValue = "yes"
        let testFalseValue = "no"
        
        XCTAssertEqual(testTrueValue.toBool(trueValue: testTrueValue, falseValue: testFalseValue), true)
        XCTAssertEqual(testFalseValue.toBool(trueValue: testTrueValue, falseValue: testFalseValue), false)
        XCTAssertNil("wrong".toBool(trueValue: testTrueValue, falseValue: testFalseValue))
        XCTAssertNoThrow(try testTrueValue.toBoolOrThrow(trueValue: testTrueValue, falseValue: testFalseValue), "")
        XCTAssertNoThrow(try testFalseValue.toBoolOrThrow(trueValue: testTrueValue, falseValue: testFalseValue), "")
        XCTAssertThrowsError(try "wrong".toBoolOrThrow(trueValue: testTrueValue, falseValue: testFalseValue))
    }
    
    func testStringToURL() {
        let correctURL1 = "http://localhost:4000/api/Test"
        let correctURL2 = "https://localhost:4000/api/Test"
        let correctURL3 = "/api/Test"
        let correctURL4 = "htt://localhost:4000/api/Test"
        let wrongURL1 = ""
        let wrongURL2 = "bad url"
        
        
        XCTAssertNil(wrongURL1.toURL)
        XCTAssertNil(wrongURL2.toURL)
        
        XCTAssertNotNil(correctURL1.toURL)
        XCTAssertNotNil(correctURL2.toURL)
        XCTAssertNotNil(correctURL3.toURL)
        XCTAssertNotNil(correctURL4.toURL)
        
    }
    
    func testSafeArray() {
        let array = ["test1","test2","test3"]
        
        XCTAssertNil(array[safe: -1])
        XCTAssertNil(array[safe: 3])
        XCTAssertEqual(array[safe:0], array.first)
    }
}
