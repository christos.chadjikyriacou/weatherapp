//
//  JSONSamples.swift
//  WeatherTests
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import Foundation


class JSONSamples {
    
    static let citiesSearchResultJSON = """
    {
        "search_api": {
            "result": [
                {
                    "areaName": [
                        {
                            "value": "Alaykoy"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.183",
                    "longitude": "33.250",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1833,33.25"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Eskikuyu"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.183",
                    "longitude": "33.250",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1833,33.25"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Yerolakkos"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.183",
                    "longitude": "33.250",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1833,33.25"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Yerolakko"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.183",
                    "longitude": "33.250",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1833,33.25"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Asagaâ± Lakadamya"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.121",
                    "longitude": "33.317",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1208,33.3167"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Asagaâ± Lakatamya"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.121",
                    "longitude": "33.317",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1208,33.3167"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Kato Lakatamia"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.121",
                    "longitude": "33.317",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1208,33.3167"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Kato Lakatamya"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.121",
                    "longitude": "33.317",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1208,33.3167"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Kato Lakatameia"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.121",
                    "longitude": "33.317",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1208,33.3167"
                        }
                    ]
                },
                {
                    "areaName": [
                        {
                            "value": "Kato Lakkatamia"
                        }
                    ],
                    "country": [
                        {
                            "value": "Cyprus"
                        }
                    ],
                    "region": [
                        {
                            "value": "Nicosia"
                        }
                    ],
                    "latitude": "35.121",
                    "longitude": "33.317",
                    "population": "0",
                    "weatherUrl": [
                        {
                            "value": "https://www.worldweatheronline.com/v2/weather.aspx?q=35.1208,33.3167"
                        }
                    ]
                }
            ]
        }
    }
    """
    
    
    static let cityWeatherSampleJSON:String =
    """
    {
        "data": {
            "request": [
                {
                    "type": "City",
                    "query": "Nicosia, Cyprus"
                }
            ],
            "current_condition": [
                {
                    "observation_time": "12:54 PM",
                    "isdaytime": "yes",
                    "temp_C": "38",
                    "temp_F": "100",
                    "weatherCode": "116",
                    "weatherIconUrl": [
                        {
                            "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                        }
                    ],
                    "weatherDesc": [
                        {
                            "value": "Partly cloudy"
                        }
                    ],
                    "windspeedMiles": "11",
                    "windspeedKmph": "17",
                    "winddirDegree": "170",
                    "winddir16Point": "S",
                    "precipMM": "0.0",
                    "precipInches": "0.0",
                    "humidity": "27",
                    "visibility": "10",
                    "visibilityMiles": "6",
                    "pressure": "1002",
                    "pressureInches": "30",
                    "cloudcover": "25",
                    "FeelsLikeC": "44",
                    "FeelsLikeF": "110",
                    "uvIndex": "8"
                }
            ],
            "weather": [
                {
                    "date": "2020-07-12",
                    "astronomy": [
                        {
                            "sunrise": "05:42 AM",
                            "sunset": "08:02 PM",
                            "moonrise": "12:08 AM",
                            "moonset": "12:25 PM",
                            "moon_phase": "Waning Crescent",
                            "moon_illumination": "48"
                        }
                    ],
                    "maxtempC": "36",
                    "maxtempF": "97",
                    "mintempC": "22",
                    "mintempF": "72",
                    "avgtempC": "29",
                    "avgtempF": "83",
                    "totalSnow_cm": "0.0",
                    "sunHour": "14.5",
                    "uvIndex": "11",
                    "hourly": [
                        {
                            "time": "0",
                            "isdaytime": "no",
                            "tempC": "24",
                            "tempF": "75",
                            "windspeedMiles": "11",
                            "windspeedKmph": "18",
                            "winddirDegree": "270",
                            "winddir16Point": "W",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "68",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "1",
                            "HeatIndexC": "26",
                            "HeatIndexF": "78",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "24",
                            "WindChillF": "75",
                            "WindGustMiles": "23",
                            "WindGustKmph": "37",
                            "FeelsLikeC": "26",
                            "FeelsLikeF": "78",
                            "chanceofrain": "0",
                            "chanceofremdry": "84",
                            "chanceofwindy": "0",
                            "chanceofovercast": "12",
                            "chanceofsunshine": "84",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "10",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "300",
                            "isdaytime": "no",
                            "tempC": "23",
                            "tempF": "73",
                            "windspeedMiles": "7",
                            "windspeedKmph": "11",
                            "winddirDegree": "276",
                            "winddir16Point": "W",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "76",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "3",
                            "HeatIndexC": "25",
                            "HeatIndexF": "77",
                            "DewPointC": "18",
                            "DewPointF": "65",
                            "WindChillC": "23",
                            "WindChillF": "73",
                            "WindGustMiles": "14",
                            "WindGustKmph": "23",
                            "FeelsLikeC": "25",
                            "FeelsLikeF": "77",
                            "chanceofrain": "0",
                            "chanceofremdry": "83",
                            "chanceofwindy": "0",
                            "chanceofovercast": "35",
                            "chanceofsunshine": "76",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "0",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "600",
                            "isdaytime": "yes",
                            "tempC": "25",
                            "tempF": "77",
                            "windspeedMiles": "5",
                            "windspeedKmph": "9",
                            "winddirDegree": "304",
                            "winddir16Point": "WNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "70",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "4",
                            "HeatIndexC": "27",
                            "HeatIndexF": "80",
                            "DewPointC": "18",
                            "DewPointF": "65",
                            "WindChillC": "25",
                            "WindChillF": "77",
                            "WindGustMiles": "9",
                            "WindGustKmph": "15",
                            "FeelsLikeC": "27",
                            "FeelsLikeF": "80",
                            "chanceofrain": "0",
                            "chanceofremdry": "83",
                            "chanceofwindy": "0",
                            "chanceofovercast": "37",
                            "chanceofsunshine": "80",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "33",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "6"
                        },
                        {
                            "time": "900",
                            "isdaytime": "yes",
                            "tempC": "32",
                            "tempF": "89",
                            "windspeedMiles": "5",
                            "windspeedKmph": "8",
                            "winddirDegree": "326",
                            "winddir16Point": "NW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "44",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "5",
                            "HeatIndexC": "33",
                            "HeatIndexF": "92",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "32",
                            "WindChillF": "89",
                            "WindGustMiles": "6",
                            "WindGustKmph": "9",
                            "FeelsLikeC": "33",
                            "FeelsLikeF": "92",
                            "chanceofrain": "0",
                            "chanceofremdry": "85",
                            "chanceofwindy": "0",
                            "chanceofovercast": "41",
                            "chanceofsunshine": "80",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "98",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1200",
                            "isdaytime": "yes",
                            "tempC": "35",
                            "tempF": "96",
                            "windspeedMiles": "3",
                            "windspeedKmph": "5",
                            "winddirDegree": "306",
                            "winddir16Point": "NW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "33",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "5",
                            "HeatIndexC": "37",
                            "HeatIndexF": "98",
                            "DewPointC": "17",
                            "DewPointF": "62",
                            "WindChillC": "35",
                            "WindChillF": "96",
                            "WindGustMiles": "4",
                            "WindGustKmph": "6",
                            "FeelsLikeC": "37",
                            "FeelsLikeF": "98",
                            "chanceofrain": "0",
                            "chanceofremdry": "87",
                            "chanceofwindy": "0",
                            "chanceofovercast": "33",
                            "chanceofsunshine": "80",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "98",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "9"
                        },
                        {
                            "time": "1500",
                            "isdaytime": "yes",
                            "tempC": "38",
                            "tempF": "100",
                            "windspeedMiles": "11",
                            "windspeedKmph": "17",
                            "winddirDegree": "170",
                            "winddir16Point": "S",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "27",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "25",
                            "HeatIndexC": "36",
                            "HeatIndexF": "98",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "35",
                            "WindChillF": "94",
                            "WindGustMiles": "11",
                            "WindGustKmph": "17",
                            "FeelsLikeC": "36",
                            "FeelsLikeF": "98",
                            "chanceofrain": "0",
                            "chanceofremdry": "86",
                            "chanceofwindy": "0",
                            "chanceofovercast": "23",
                            "chanceofsunshine": "85",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "97",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1800",
                            "isdaytime": "yes",
                            "tempC": "30",
                            "tempF": "87",
                            "windspeedMiles": "15",
                            "windspeedKmph": "24",
                            "winddirDegree": "296",
                            "winddir16Point": "WNW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "61",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "33",
                            "HeatIndexF": "91",
                            "DewPointC": "21",
                            "DewPointF": "69",
                            "WindChillC": "30",
                            "WindChillF": "87",
                            "WindGustMiles": "21",
                            "WindGustKmph": "34",
                            "FeelsLikeC": "33",
                            "FeelsLikeF": "91",
                            "chanceofrain": "0",
                            "chanceofremdry": "86",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "88",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "74",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "2100",
                            "isdaytime": "no",
                            "tempC": "26",
                            "tempF": "79",
                            "windspeedMiles": "11",
                            "windspeedKmph": "18",
                            "winddirDegree": "271",
                            "winddir16Point": "W",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Clear"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "79",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "28",
                            "HeatIndexF": "83",
                            "DewPointC": "20",
                            "DewPointF": "69",
                            "WindChillC": "26",
                            "WindChillF": "79",
                            "WindGustMiles": "23",
                            "WindGustKmph": "37",
                            "FeelsLikeC": "28",
                            "FeelsLikeF": "83",
                            "chanceofrain": "0",
                            "chanceofremdry": "84",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "92",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "22",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        }
                    ]
                },
                {
                    "date": "2020-07-13",
                    "astronomy": [
                        {
                            "sunrise": "05:42 AM",
                            "sunset": "08:02 PM",
                            "moonrise": "12:34 AM",
                            "moonset": "01:20 PM",
                            "moon_phase": "Waning Crescent",
                            "moon_illumination": "41"
                        }
                    ],
                    "maxtempC": "37",
                    "maxtempF": "98",
                    "mintempC": "22",
                    "mintempF": "72",
                    "avgtempC": "29",
                    "avgtempF": "85",
                    "totalSnow_cm": "0.0",
                    "sunHour": "14.5",
                    "uvIndex": "11",
                    "hourly": [
                        {
                            "time": "0",
                            "isdaytime": "no",
                            "tempC": "23",
                            "tempF": "74",
                            "windspeedMiles": "7",
                            "windspeedKmph": "11",
                            "winddirDegree": "270",
                            "winddir16Point": "W",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "75",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "1",
                            "HeatIndexC": "25",
                            "HeatIndexF": "77",
                            "DewPointC": "19",
                            "DewPointF": "65",
                            "WindChillC": "23",
                            "WindChillF": "74",
                            "WindGustMiles": "14",
                            "WindGustKmph": "23",
                            "FeelsLikeC": "25",
                            "FeelsLikeF": "77",
                            "chanceofrain": "0",
                            "chanceofremdry": "82",
                            "chanceofwindy": "0",
                            "chanceofovercast": "11",
                            "chanceofsunshine": "87",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "7",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "300",
                            "isdaytime": "no",
                            "tempC": "23",
                            "tempF": "73",
                            "windspeedMiles": "4",
                            "windspeedKmph": "6",
                            "winddirDegree": "300",
                            "winddir16Point": "WNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "74",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1001",
                            "pressureInches": "30",
                            "cloudcover": "2",
                            "HeatIndexC": "25",
                            "HeatIndexF": "77",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "23",
                            "WindChillF": "73",
                            "WindGustMiles": "8",
                            "WindGustKmph": "13",
                            "FeelsLikeC": "25",
                            "FeelsLikeF": "77",
                            "chanceofrain": "0",
                            "chanceofremdry": "86",
                            "chanceofwindy": "0",
                            "chanceofovercast": "34",
                            "chanceofsunshine": "75",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "0",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "600",
                            "isdaytime": "yes",
                            "tempC": "25",
                            "tempF": "76",
                            "windspeedMiles": "4",
                            "windspeedKmph": "7",
                            "winddirDegree": "335",
                            "winddir16Point": "NNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "76",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1001",
                            "pressureInches": "30",
                            "cloudcover": "2",
                            "HeatIndexC": "27",
                            "HeatIndexF": "80",
                            "DewPointC": "20",
                            "DewPointF": "67",
                            "WindChillC": "25",
                            "WindChillF": "76",
                            "WindGustMiles": "8",
                            "WindGustKmph": "12",
                            "FeelsLikeC": "27",
                            "FeelsLikeF": "80",
                            "chanceofrain": "0",
                            "chanceofremdry": "92",
                            "chanceofwindy": "0",
                            "chanceofovercast": "41",
                            "chanceofsunshine": "76",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "31",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "6"
                        },
                        {
                            "time": "900",
                            "isdaytime": "yes",
                            "tempC": "31",
                            "tempF": "88",
                            "windspeedMiles": "4",
                            "windspeedKmph": "7",
                            "winddirDegree": "244",
                            "winddir16Point": "WSW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "49",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "2",
                            "HeatIndexC": "33",
                            "HeatIndexF": "91",
                            "DewPointC": "19",
                            "DewPointF": "66",
                            "WindChillC": "31",
                            "WindChillF": "88",
                            "WindGustMiles": "5",
                            "WindGustKmph": "8",
                            "FeelsLikeC": "33",
                            "FeelsLikeF": "91",
                            "chanceofrain": "0",
                            "chanceofremdry": "91",
                            "chanceofwindy": "0",
                            "chanceofovercast": "43",
                            "chanceofsunshine": "86",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "94",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1200",
                            "isdaytime": "yes",
                            "tempC": "35",
                            "tempF": "96",
                            "windspeedMiles": "3",
                            "windspeedKmph": "5",
                            "winddirDegree": "138",
                            "winddir16Point": "SE",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "32",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "3",
                            "HeatIndexC": "36",
                            "HeatIndexF": "97",
                            "DewPointC": "16",
                            "DewPointF": "61",
                            "WindChillC": "35",
                            "WindChillF": "96",
                            "WindGustMiles": "4",
                            "WindGustKmph": "6",
                            "FeelsLikeC": "36",
                            "FeelsLikeF": "97",
                            "chanceofrain": "0",
                            "chanceofremdry": "86",
                            "chanceofwindy": "0",
                            "chanceofovercast": "38",
                            "chanceofsunshine": "81",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "94",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "9"
                        },
                        {
                            "time": "1500",
                            "isdaytime": "yes",
                            "tempC": "35",
                            "tempF": "96",
                            "windspeedMiles": "4",
                            "windspeedKmph": "6",
                            "winddirDegree": "333",
                            "winddir16Point": "NNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "34",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1001",
                            "pressureInches": "30",
                            "cloudcover": "3",
                            "HeatIndexC": "37",
                            "HeatIndexF": "98",
                            "DewPointC": "17",
                            "DewPointF": "62",
                            "WindChillC": "35",
                            "WindChillF": "96",
                            "WindGustMiles": "5",
                            "WindGustKmph": "8",
                            "FeelsLikeC": "37",
                            "FeelsLikeF": "98",
                            "chanceofrain": "0",
                            "chanceofremdry": "81",
                            "chanceofwindy": "0",
                            "chanceofovercast": "40",
                            "chanceofsunshine": "73",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "93",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "9"
                        },
                        {
                            "time": "1800",
                            "isdaytime": "yes",
                            "tempC": "32",
                            "tempF": "90",
                            "windspeedMiles": "9",
                            "windspeedKmph": "15",
                            "winddirDegree": "299",
                            "winddir16Point": "WNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "47",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1001",
                            "pressureInches": "30",
                            "cloudcover": "3",
                            "HeatIndexC": "34",
                            "HeatIndexF": "94",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "32",
                            "WindChillF": "90",
                            "WindGustMiles": "13",
                            "WindGustKmph": "21",
                            "FeelsLikeC": "34",
                            "FeelsLikeF": "94",
                            "chanceofrain": "0",
                            "chanceofremdry": "83",
                            "chanceofwindy": "0",
                            "chanceofovercast": "39",
                            "chanceofsunshine": "73",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "90",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "2100",
                            "isdaytime": "no",
                            "tempC": "29",
                            "tempF": "84",
                            "windspeedMiles": "8",
                            "windspeedKmph": "13",
                            "winddirDegree": "282",
                            "winddir16Point": "WNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "64",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "4",
                            "HeatIndexC": "32",
                            "HeatIndexF": "89",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "29",
                            "WindChillF": "84",
                            "WindGustMiles": "17",
                            "WindGustKmph": "27",
                            "FeelsLikeC": "32",
                            "FeelsLikeF": "89",
                            "chanceofrain": "0",
                            "chanceofremdry": "83",
                            "chanceofwindy": "0",
                            "chanceofovercast": "33",
                            "chanceofsunshine": "79",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "60",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        }
                    ]
                },
                {
                    "date": "2020-07-14",
                    "astronomy": [
                        {
                            "sunrise": "05:43 AM",
                            "sunset": "08:01 PM",
                            "moonrise": "01:01 AM",
                            "moonset": "02:16 PM",
                            "moon_phase": "Waning Crescent",
                            "moon_illumination": "34"
                        }
                    ],
                    "maxtempC": "36",
                    "maxtempF": "97",
                    "mintempC": "23",
                    "mintempF": "73",
                    "avgtempC": "30",
                    "avgtempF": "86",
                    "totalSnow_cm": "0.0",
                    "sunHour": "14.5",
                    "uvIndex": "11",
                    "hourly": [
                        {
                            "time": "0",
                            "isdaytime": "no",
                            "tempC": "25",
                            "tempF": "77",
                            "windspeedMiles": "6",
                            "windspeedKmph": "10",
                            "winddirDegree": "282",
                            "winddir16Point": "WNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "74",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "4",
                            "HeatIndexC": "28",
                            "HeatIndexF": "82",
                            "DewPointC": "18",
                            "DewPointF": "65",
                            "WindChillC": "25",
                            "WindChillF": "77",
                            "WindGustMiles": "13",
                            "WindGustKmph": "21",
                            "FeelsLikeC": "28",
                            "FeelsLikeF": "82",
                            "chanceofrain": "0",
                            "chanceofremdry": "83",
                            "chanceofwindy": "0",
                            "chanceofovercast": "39",
                            "chanceofsunshine": "77",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "7",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "300",
                            "isdaytime": "no",
                            "tempC": "23",
                            "tempF": "73",
                            "windspeedMiles": "4",
                            "windspeedKmph": "7",
                            "winddirDegree": "297",
                            "winddir16Point": "WNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "74",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "4",
                            "HeatIndexC": "25",
                            "HeatIndexF": "77",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "23",
                            "WindChillF": "73",
                            "WindGustMiles": "9",
                            "WindGustKmph": "15",
                            "FeelsLikeC": "25",
                            "FeelsLikeF": "77",
                            "chanceofrain": "0",
                            "chanceofremdry": "86",
                            "chanceofwindy": "0",
                            "chanceofovercast": "43",
                            "chanceofsunshine": "76",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "0",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "600",
                            "isdaytime": "yes",
                            "tempC": "25",
                            "tempF": "78",
                            "windspeedMiles": "4",
                            "windspeedKmph": "7",
                            "winddirDegree": "213",
                            "winddir16Point": "SSW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "68",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1003",
                            "pressureInches": "30",
                            "cloudcover": "4",
                            "HeatIndexC": "28",
                            "HeatIndexF": "82",
                            "DewPointC": "19",
                            "DewPointF": "66",
                            "WindChillC": "25",
                            "WindChillF": "78",
                            "WindGustMiles": "7",
                            "WindGustKmph": "12",
                            "FeelsLikeC": "28",
                            "FeelsLikeF": "82",
                            "chanceofrain": "0",
                            "chanceofremdry": "83",
                            "chanceofwindy": "0",
                            "chanceofovercast": "41",
                            "chanceofsunshine": "80",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "30",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "7"
                        },
                        {
                            "time": "900",
                            "isdaytime": "yes",
                            "tempC": "32",
                            "tempF": "90",
                            "windspeedMiles": "7",
                            "windspeedKmph": "12",
                            "winddirDegree": "33",
                            "winddir16Point": "NNE",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "43",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "4",
                            "HeatIndexC": "34",
                            "HeatIndexF": "92",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "32",
                            "WindChillF": "90",
                            "WindGustMiles": "9",
                            "WindGustKmph": "14",
                            "FeelsLikeC": "34",
                            "FeelsLikeF": "92",
                            "chanceofrain": "0",
                            "chanceofremdry": "87",
                            "chanceofwindy": "0",
                            "chanceofovercast": "43",
                            "chanceofsunshine": "76",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "90",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1200",
                            "isdaytime": "yes",
                            "tempC": "35",
                            "tempF": "96",
                            "windspeedMiles": "9",
                            "windspeedKmph": "14",
                            "winddirDegree": "62",
                            "winddir16Point": "ENE",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "31",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "4",
                            "HeatIndexC": "36",
                            "HeatIndexF": "97",
                            "DewPointC": "16",
                            "DewPointF": "60",
                            "WindChillC": "35",
                            "WindChillF": "96",
                            "WindGustMiles": "10",
                            "WindGustKmph": "17",
                            "FeelsLikeC": "36",
                            "FeelsLikeF": "97",
                            "chanceofrain": "0",
                            "chanceofremdry": "90",
                            "chanceofwindy": "0",
                            "chanceofovercast": "45",
                            "chanceofsunshine": "77",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "89",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "9"
                        },
                        {
                            "time": "1500",
                            "isdaytime": "yes",
                            "tempC": "35",
                            "tempF": "95",
                            "windspeedMiles": "6",
                            "windspeedKmph": "9",
                            "winddirDegree": "161",
                            "winddir16Point": "SSE",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "35",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "4",
                            "HeatIndexC": "37",
                            "HeatIndexF": "98",
                            "DewPointC": "17",
                            "DewPointF": "63",
                            "WindChillC": "35",
                            "WindChillF": "95",
                            "WindGustMiles": "7",
                            "WindGustKmph": "11",
                            "FeelsLikeC": "37",
                            "FeelsLikeF": "98",
                            "chanceofrain": "0",
                            "chanceofremdry": "91",
                            "chanceofwindy": "0",
                            "chanceofovercast": "37",
                            "chanceofsunshine": "84",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "89",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "9"
                        },
                        {
                            "time": "1800",
                            "isdaytime": "yes",
                            "tempC": "32",
                            "tempF": "90",
                            "windspeedMiles": "6",
                            "windspeedKmph": "9",
                            "winddirDegree": "303",
                            "winddir16Point": "WNW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "52",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "2",
                            "HeatIndexC": "34",
                            "HeatIndexF": "94",
                            "DewPointC": "19",
                            "DewPointF": "66",
                            "WindChillC": "32",
                            "WindChillF": "90",
                            "WindGustMiles": "9",
                            "WindGustKmph": "14",
                            "FeelsLikeC": "34",
                            "FeelsLikeF": "94",
                            "chanceofrain": "0",
                            "chanceofremdry": "88",
                            "chanceofwindy": "0",
                            "chanceofovercast": "26",
                            "chanceofsunshine": "91",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "85",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "2100",
                            "isdaytime": "no",
                            "tempC": "29",
                            "tempF": "85",
                            "windspeedMiles": "9",
                            "windspeedKmph": "14",
                            "winddirDegree": "289",
                            "winddir16Point": "WNW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Clear"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "71",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "32",
                            "HeatIndexF": "90",
                            "DewPointC": "19",
                            "DewPointF": "67",
                            "WindChillC": "29",
                            "WindChillF": "85",
                            "WindGustMiles": "18",
                            "WindGustKmph": "28",
                            "FeelsLikeC": "32",
                            "FeelsLikeF": "90",
                            "chanceofrain": "0",
                            "chanceofremdry": "90",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "93",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "56",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        }
                    ]
                },
                {
                    "date": "2020-07-15",
                    "astronomy": [
                        {
                            "sunrise": "05:44 AM",
                            "sunset": "08:01 PM",
                            "moonrise": "01:30 AM",
                            "moonset": "03:13 PM",
                            "moon_phase": "Waning Crescent",
                            "moon_illumination": "28"
                        }
                    ],
                    "maxtempC": "36",
                    "maxtempF": "96",
                    "mintempC": "23",
                    "mintempF": "73",
                    "avgtempC": "30",
                    "avgtempF": "86",
                    "totalSnow_cm": "0.0",
                    "sunHour": "14.5",
                    "uvIndex": "11",
                    "hourly": [
                        {
                            "time": "0",
                            "isdaytime": "no",
                            "tempC": "27",
                            "tempF": "80",
                            "windspeedMiles": "11",
                            "windspeedKmph": "18",
                            "winddirDegree": "277",
                            "winddir16Point": "W",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Clear"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "67",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "30",
                            "HeatIndexF": "86",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "27",
                            "WindChillF": "80",
                            "WindGustMiles": "23",
                            "WindGustKmph": "37",
                            "FeelsLikeC": "30",
                            "FeelsLikeF": "86",
                            "chanceofrain": "0",
                            "chanceofremdry": "88",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "92",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "16",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "300",
                            "isdaytime": "no",
                            "tempC": "24",
                            "tempF": "75",
                            "windspeedMiles": "10",
                            "windspeedKmph": "16",
                            "winddirDegree": "273",
                            "winddir16Point": "W",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "61",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "1",
                            "HeatIndexC": "26",
                            "HeatIndexF": "78",
                            "DewPointC": "16",
                            "DewPointF": "60",
                            "WindChillC": "24",
                            "WindChillF": "75",
                            "WindGustMiles": "21",
                            "WindGustKmph": "34",
                            "FeelsLikeC": "26",
                            "FeelsLikeF": "78",
                            "chanceofrain": "0",
                            "chanceofremdry": "85",
                            "chanceofwindy": "0",
                            "chanceofovercast": "12",
                            "chanceofsunshine": "88",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "12",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "600",
                            "isdaytime": "yes",
                            "tempC": "26",
                            "tempF": "78",
                            "windspeedMiles": "4",
                            "windspeedKmph": "6",
                            "winddirDegree": "286",
                            "winddir16Point": "WNW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "57",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "1",
                            "HeatIndexC": "27",
                            "HeatIndexF": "81",
                            "DewPointC": "16",
                            "DewPointF": "61",
                            "WindChillC": "26",
                            "WindChillF": "78",
                            "WindGustMiles": "6",
                            "WindGustKmph": "10",
                            "FeelsLikeC": "27",
                            "FeelsLikeF": "81",
                            "chanceofrain": "0",
                            "chanceofremdry": "86",
                            "chanceofwindy": "0",
                            "chanceofovercast": "24",
                            "chanceofsunshine": "82",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "29",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "7"
                        },
                        {
                            "time": "900",
                            "isdaytime": "yes",
                            "tempC": "33",
                            "tempF": "92",
                            "windspeedMiles": "7",
                            "windspeedKmph": "12",
                            "winddirDegree": "314",
                            "winddir16Point": "NW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "36",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1006",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "34",
                            "HeatIndexF": "93",
                            "DewPointC": "16",
                            "DewPointF": "61",
                            "WindChillC": "33",
                            "WindChillF": "92",
                            "WindGustMiles": "9",
                            "WindGustKmph": "14",
                            "FeelsLikeC": "34",
                            "FeelsLikeF": "93",
                            "chanceofrain": "0",
                            "chanceofremdry": "88",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "88",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "86",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1200",
                            "isdaytime": "yes",
                            "tempC": "36",
                            "tempF": "96",
                            "windspeedMiles": "7",
                            "windspeedKmph": "11",
                            "winddirDegree": "326",
                            "winddir16Point": "NW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "31",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1006",
                            "pressureInches": "30",
                            "cloudcover": "1",
                            "HeatIndexC": "36",
                            "HeatIndexF": "98",
                            "DewPointC": "16",
                            "DewPointF": "61",
                            "WindChillC": "36",
                            "WindChillF": "96",
                            "WindGustMiles": "8",
                            "WindGustKmph": "13",
                            "FeelsLikeC": "36",
                            "FeelsLikeF": "98",
                            "chanceofrain": "0",
                            "chanceofremdry": "90",
                            "chanceofwindy": "0",
                            "chanceofovercast": "15",
                            "chanceofsunshine": "87",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "85",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "9"
                        },
                        {
                            "time": "1500",
                            "isdaytime": "yes",
                            "tempC": "35",
                            "tempF": "94",
                            "windspeedMiles": "10",
                            "windspeedKmph": "16",
                            "winddirDegree": "313",
                            "winddir16Point": "NW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "32",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1006",
                            "pressureInches": "30",
                            "cloudcover": "2",
                            "HeatIndexC": "35",
                            "HeatIndexF": "96",
                            "DewPointC": "16",
                            "DewPointF": "60",
                            "WindChillC": "35",
                            "WindChillF": "94",
                            "WindGustMiles": "11",
                            "WindGustKmph": "18",
                            "FeelsLikeC": "35",
                            "FeelsLikeF": "96",
                            "chanceofrain": "0",
                            "chanceofremdry": "83",
                            "chanceofwindy": "0",
                            "chanceofovercast": "30",
                            "chanceofsunshine": "89",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "85",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1800",
                            "isdaytime": "yes",
                            "tempC": "32",
                            "tempF": "90",
                            "windspeedMiles": "14",
                            "windspeedKmph": "23",
                            "winddirDegree": "294",
                            "winddir16Point": "WNW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "46",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1006",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "34",
                            "HeatIndexF": "93",
                            "DewPointC": "16",
                            "DewPointF": "61",
                            "WindChillC": "32",
                            "WindChillF": "90",
                            "WindGustMiles": "20",
                            "WindGustKmph": "33",
                            "FeelsLikeC": "34",
                            "FeelsLikeF": "93",
                            "chanceofrain": "0",
                            "chanceofremdry": "87",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "92",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "74",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "2100",
                            "isdaytime": "no",
                            "tempC": "30",
                            "tempF": "85",
                            "windspeedMiles": "13",
                            "windspeedKmph": "21",
                            "winddirDegree": "279",
                            "winddir16Point": "W",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Clear"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "69",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1006",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "33",
                            "HeatIndexF": "92",
                            "DewPointC": "18",
                            "DewPointF": "65",
                            "WindChillC": "30",
                            "WindChillF": "85",
                            "WindGustMiles": "28",
                            "WindGustKmph": "45",
                            "FeelsLikeC": "33",
                            "FeelsLikeF": "92",
                            "chanceofrain": "0",
                            "chanceofremdry": "89",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "90",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "38",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        }
                    ]
                },
                {
                    "date": "2020-07-16",
                    "astronomy": [
                        {
                            "sunrise": "05:44 AM",
                            "sunset": "08:01 PM",
                            "moonrise": "02:02 AM",
                            "moonset": "04:12 PM",
                            "moon_phase": "Waning Crescent",
                            "moon_illumination": "21"
                        }
                    ],
                    "maxtempC": "34",
                    "maxtempF": "94",
                    "mintempC": "21",
                    "mintempF": "71",
                    "avgtempC": "28",
                    "avgtempF": "82",
                    "totalSnow_cm": "0.0",
                    "sunHour": "14.5",
                    "uvIndex": "11",
                    "hourly": [
                        {
                            "time": "0",
                            "isdaytime": "no",
                            "tempC": "25",
                            "tempF": "78",
                            "windspeedMiles": "12",
                            "windspeedKmph": "19",
                            "winddirDegree": "272",
                            "winddir16Point": "W",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Clear"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "71",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "28",
                            "HeatIndexF": "83",
                            "DewPointC": "18",
                            "DewPointF": "64",
                            "WindChillC": "25",
                            "WindChillF": "78",
                            "WindGustMiles": "25",
                            "WindGustKmph": "41",
                            "FeelsLikeC": "28",
                            "FeelsLikeF": "83",
                            "chanceofrain": "0",
                            "chanceofremdry": "82",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "89",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "7",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "300",
                            "isdaytime": "no",
                            "tempC": "22",
                            "tempF": "72",
                            "windspeedMiles": "8",
                            "windspeedKmph": "13",
                            "winddirDegree": "272",
                            "winddir16Point": "W",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Clear"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "73",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "25",
                            "HeatIndexF": "76",
                            "DewPointC": "17",
                            "DewPointF": "63",
                            "WindChillC": "22",
                            "WindChillF": "72",
                            "WindGustMiles": "17",
                            "WindGustKmph": "27",
                            "FeelsLikeC": "25",
                            "FeelsLikeF": "76",
                            "chanceofrain": "0",
                            "chanceofremdry": "83",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "92",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "0",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        },
                        {
                            "time": "600",
                            "isdaytime": "yes",
                            "tempC": "24",
                            "tempF": "76",
                            "windspeedMiles": "2",
                            "windspeedKmph": "4",
                            "winddirDegree": "281",
                            "winddir16Point": "W",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "63",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "27",
                            "HeatIndexF": "80",
                            "DewPointC": "17",
                            "DewPointF": "62",
                            "WindChillC": "24",
                            "WindChillF": "76",
                            "WindGustMiles": "4",
                            "WindGustKmph": "7",
                            "FeelsLikeC": "27",
                            "FeelsLikeF": "80",
                            "chanceofrain": "0",
                            "chanceofremdry": "86",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "90",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "27",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "6"
                        },
                        {
                            "time": "900",
                            "isdaytime": "yes",
                            "tempC": "32",
                            "tempF": "89",
                            "windspeedMiles": "1",
                            "windspeedKmph": "2",
                            "winddirDegree": "236",
                            "winddir16Point": "SW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "42",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1005",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "32",
                            "HeatIndexF": "90",
                            "DewPointC": "17",
                            "DewPointF": "62",
                            "WindChillC": "32",
                            "WindChillF": "89",
                            "WindGustMiles": "2",
                            "WindGustKmph": "3",
                            "FeelsLikeC": "32",
                            "FeelsLikeF": "90",
                            "chanceofrain": "0",
                            "chanceofremdry": "85",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "92",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "82",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1200",
                            "isdaytime": "yes",
                            "tempC": "34",
                            "tempF": "93",
                            "windspeedMiles": "4",
                            "windspeedKmph": "6",
                            "winddirDegree": "190",
                            "winddir16Point": "S",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "36",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1004",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "35",
                            "HeatIndexF": "94",
                            "DewPointC": "17",
                            "DewPointF": "62",
                            "WindChillC": "34",
                            "WindChillF": "93",
                            "WindGustMiles": "4",
                            "WindGustKmph": "7",
                            "FeelsLikeC": "35",
                            "FeelsLikeF": "94",
                            "chanceofrain": "0",
                            "chanceofremdry": "84",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "90",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "81",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1500",
                            "isdaytime": "yes",
                            "tempC": "32",
                            "tempF": "90",
                            "windspeedMiles": "9",
                            "windspeedKmph": "15",
                            "winddirDegree": "298",
                            "winddir16Point": "WNW",
                            "weatherCode": "116",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Partly cloudy"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "45",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1003",
                            "pressureInches": "30",
                            "cloudcover": "1",
                            "HeatIndexC": "34",
                            "HeatIndexF": "93",
                            "DewPointC": "18",
                            "DewPointF": "65",
                            "WindChillC": "32",
                            "WindChillF": "90",
                            "WindGustMiles": "11",
                            "WindGustKmph": "17",
                            "FeelsLikeC": "34",
                            "FeelsLikeF": "93",
                            "chanceofrain": "0",
                            "chanceofremdry": "86",
                            "chanceofwindy": "0",
                            "chanceofovercast": "13",
                            "chanceofsunshine": "88",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "80",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "8"
                        },
                        {
                            "time": "1800",
                            "isdaytime": "yes",
                            "tempC": "30",
                            "tempF": "85",
                            "windspeedMiles": "11",
                            "windspeedKmph": "17",
                            "winddirDegree": "303",
                            "winddir16Point": "WNW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Sunny"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "60",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "2",
                            "HeatIndexC": "32",
                            "HeatIndexF": "90",
                            "DewPointC": "19",
                            "DewPointF": "67",
                            "WindChillC": "30",
                            "WindChillF": "85",
                            "WindGustMiles": "13",
                            "WindGustKmph": "22",
                            "FeelsLikeC": "32",
                            "FeelsLikeF": "90",
                            "chanceofrain": "0",
                            "chanceofremdry": "88",
                            "chanceofwindy": "0",
                            "chanceofovercast": "26",
                            "chanceofsunshine": "81",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "57",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "7"
                        },
                        {
                            "time": "2100",
                            "isdaytime": "no",
                            "tempC": "27",
                            "tempF": "80",
                            "windspeedMiles": "8",
                            "windspeedKmph": "12",
                            "winddirDegree": "292",
                            "winddir16Point": "WNW",
                            "weatherCode": "113",
                            "weatherIconUrl": [
                                {
                                    "value": "http://cdn.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png"
                                }
                            ],
                            "weatherDesc": [
                                {
                                    "value": "Clear"
                                }
                            ],
                            "precipMM": "0.0",
                            "precipInches": "0.0",
                            "humidity": "83",
                            "visibility": "10",
                            "visibilityMiles": "6",
                            "pressure": "1002",
                            "pressureInches": "30",
                            "cloudcover": "0",
                            "HeatIndexC": "30",
                            "HeatIndexF": "87",
                            "DewPointC": "20",
                            "DewPointF": "68",
                            "WindChillC": "27",
                            "WindChillF": "80",
                            "WindGustMiles": "14",
                            "WindGustKmph": "22",
                            "FeelsLikeC": "30",
                            "FeelsLikeF": "87",
                            "chanceofrain": "0",
                            "chanceofremdry": "88",
                            "chanceofwindy": "0",
                            "chanceofovercast": "0",
                            "chanceofsunshine": "91",
                            "chanceoffrost": "0",
                            "chanceofhightemp": "7",
                            "chanceoffog": "0",
                            "chanceofsnow": "0",
                            "chanceofthunder": "0",
                            "uvIndex": "1"
                        }
                    ]
                }
            ],
            "ClimateAverages": [
                {
                    "month": [
                        {
                            "index": "1",
                            "name": "January",
                            "avgMinTemp": "9.9",
                            "avgMinTemp_F": "49.8",
                            "absMaxTemp": "15.807194",
                            "absMaxTemp_F": "60.5",
                            "avgDailyRainfall": "1.05"
                        },
                        {
                            "index": "2",
                            "name": "February",
                            "avgMinTemp": "9.8",
                            "avgMinTemp_F": "49.6",
                            "absMaxTemp": "18.621794",
                            "absMaxTemp_F": "65.5",
                            "avgDailyRainfall": "0.96"
                        },
                        {
                            "index": "3",
                            "name": "March",
                            "avgMinTemp": "10.6",
                            "avgMinTemp_F": "51.1",
                            "absMaxTemp": "20.229425",
                            "absMaxTemp_F": "68.4",
                            "avgDailyRainfall": "0.57"
                        },
                        {
                            "index": "4",
                            "name": "April",
                            "avgMinTemp": "13.8",
                            "avgMinTemp_F": "56.8",
                            "absMaxTemp": "23.600857",
                            "absMaxTemp_F": "74.5",
                            "avgDailyRainfall": "0.55"
                        },
                        {
                            "index": "5",
                            "name": "May",
                            "avgMinTemp": "18.5",
                            "avgMinTemp_F": "65.3",
                            "absMaxTemp": "29.629032",
                            "absMaxTemp_F": "85.3",
                            "avgDailyRainfall": "0.40"
                        },
                        {
                            "index": "6",
                            "name": "June",
                            "avgMinTemp": "22.5",
                            "avgMinTemp_F": "72.4",
                            "absMaxTemp": "31.636667",
                            "absMaxTemp_F": "88.9",
                            "avgDailyRainfall": "0.22"
                        },
                        {
                            "index": "7",
                            "name": "July",
                            "avgMinTemp": "25.0",
                            "avgMinTemp_F": "77.1",
                            "absMaxTemp": "34.182903",
                            "absMaxTemp_F": "93.5",
                            "avgDailyRainfall": "0.02"
                        },
                        {
                            "index": "8",
                            "name": "August",
                            "avgMinTemp": "25.5",
                            "avgMinTemp_F": "77.9",
                            "absMaxTemp": "35.902676",
                            "absMaxTemp_F": "96.6",
                            "avgDailyRainfall": "0.05"
                        },
                        {
                            "index": "9",
                            "name": "September",
                            "avgMinTemp": "23.4",
                            "avgMinTemp_F": "74.1",
                            "absMaxTemp": "32.528435",
                            "absMaxTemp_F": "90.6",
                            "avgDailyRainfall": "0.25"
                        },
                        {
                            "index": "10",
                            "name": "October",
                            "avgMinTemp": "20.5",
                            "avgMinTemp_F": "68.8",
                            "absMaxTemp": "27.587097",
                            "absMaxTemp_F": "81.7",
                            "avgDailyRainfall": "0.82"
                        },
                        {
                            "index": "11",
                            "name": "November",
                            "avgMinTemp": "16.2",
                            "avgMinTemp_F": "61.2",
                            "absMaxTemp": "24.182766",
                            "absMaxTemp_F": "75.5",
                            "avgDailyRainfall": "0.53"
                        },
                        {
                            "index": "12",
                            "name": "December",
                            "avgMinTemp": "12.3",
                            "avgMinTemp_F": "54.1",
                            "absMaxTemp": "18.81829",
                            "absMaxTemp_F": "65.9",
                            "avgDailyRainfall": "1.05"
                        }
                    ]
                }
            ]
        }
    }
    """
}
