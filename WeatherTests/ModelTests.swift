//
//  ModelTests.swift
//  WeatherTests
//
//  Created by Christos Chadjikyriacou on 12/07/2020.
//  Copyright © 2020 Christos Chadjikyriacou. All rights reserved.
//

import XCTest
@testable import Weather

class ModelTests:XCTestCase {
    
    
    func testCitiesSearchResultModelModel() {
        
        guard let data = JSONSamples.citiesSearchResultJSON.data(using: .utf8) else {
            XCTFail("Cannot convert to data")
            return
        }
        
        XCTAssertNoThrow(try JSONDecoder().decode(CitiesSearchResultModel.self, from: data))
    }
    
    
    func testCityWeatherModelModel() {
        
        guard let data = JSONSamples.cityWeatherSampleJSON.data(using: .utf8) else {
            XCTFail("Cannot convert to data")
            return
        }
        
        XCTAssertNoThrow(try JSONDecoder().decode(CityWeatherModel.self, from: data))
    }
}

